# Requirements

```bash
python -V
pip install virtualenv
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Bugs

If you found bug you can create a new issue ticket from the template
