# SSP Project

This is a project which demonstrates things we learned in a SSP class

[**Project**](https://gitlab.com/skrcka/ssp_project)

## Interpreter of arithmetic expressions

Interpreter of arithmetic expressions. These expressions contain +, -, *, / operators (with common priorities and left associativity) and parentheses.

## Project layout

    mkdocs.yml    # The configuration file.asdasd
    docs/
        index.md  # The documentation homepage.
        help.md   # Requirements and help

