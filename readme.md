[![pipeline status](https://gitlab.com/skrcka/ssp_project/badges/master/pipeline.svg)](https://gitlab.com/skrcka/ssp_project/commits/master)

# SSP Project

This is a project which demonstrates things we learned in a SSP class

## Requirements

```bash
python -V
pip install virtualenv
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Usage
Interpreter of arithmetic expressions. These expressions contain +, -, *, / operators (with common priorities and left associativity) and parentheses.

To simplify the task, consider we have only binary operators. There are no unary operators in our language. Moreover, we can use only positive integers in our expressions.

### Input specification
The first line of the input contains a number N. It defines the number of expressions your program should evaluate. These expressions are on next N lines. Each line contains exactly one expression.

### Output specification
For each expression write one line containing the result – the computed value of the expression. If there is any error in the input, write text ERROR instead.

### Example

- Input
```bash
2
2 * (3+5)
15 - 2**7
```
- Output
```bash
16
ERROR
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Contributors
@skrcka
@low_on_cuties

## License
[MIT](https://choosealicense.com/licenses/mit/)
