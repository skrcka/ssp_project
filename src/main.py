import re

# pylint: disable=C0321,C0301,C0103
if __name__ == "__main__":
    terminals = []
    nonterminals = []
    emptynonterminals = []
    rules = {}
    first = ''

    with open('C:\\Users\\krcek\\personal\\PJP\\3\\input.txt') as f:
        for line in f:
            line = re.sub('{.*}', '', line)
            line = re.sub(r'\s', '', line)
            line = re.sub(';', '', line)
            if len(line) == 0:
                continue
            for c in line:
                if c.islower():
                    terminals.append(c)
            nonterm, *rule = line.split(':')
            if not first:
                first = nonterm
            nonterminals.append(nonterm)
            allrules = rule[0].split('|')
            rules[nonterm] = allrules

        terminals = set(terminals)
        nonterminals = set(nonterminals)

        print(terminals)
        print(nonterminals)

        for nonterm in nonterminals:
            if '' in rules[nonterm]:
                emptynonterminals.append(nonterm)

        cnt = 0
        while cnt != len(emptynonterminals):
            cnt = len(emptynonterminals)

            for nonterm in nonterminals:
                for rule in rules[nonterm]:
                    OK = True
                    for c in rule:
                        if c in terminals:
                            OK = False
                        if c in nonterminals:
                            if c not in emptynonterminals:
                                OK = False
                    if OK and nonterm not in emptynonterminals:
                        emptynonterminals.append(nonterm)

        first_relations = {}
        for nonterm in nonterminals:
            first_relations[nonterm] = dict(list(zip(nonterminals, [False for _ in range(len(
                nonterminals))])) + list(zip(terminals, [False for _ in range(len(terminals))])))

        for nonterm in nonterminals:
            for rule in rules[nonterm]:
                if len(rule) == 0:
                    continue
                ind = 0
                c = rule[ind]
                first_relations[nonterm][c] = True
                while c in emptynonterminals:
                    ind += 1
                    if ind > len(rule)-1:
                        break
                    c = rule[ind]
                    first_relations[nonterm][c] = True
        change = True
        while change:
            change = False
            for nonterm in nonterminals:
                for col in nonterminals:
                    if first_relations[nonterm][col]:
                        for col_check in list(nonterminals) + list(terminals):
                            if first_relations[col][col_check] and not first_relations[nonterm][col_check]:
                                change = True
                                first_relations[nonterm][col_check] = True

        follow_relations = {}

        for nonterm in nonterminals:
            follow_relations[nonterm] = dict(list(zip(nonterminals, [False for _ in range(
                len(nonterminals))])) + list(zip(terminals, [False for _ in range(len(terminals))])))
            follow_relations[nonterm]['epsilon'] = False

        follow_relations[first]['epsilon'] = True

        for nonterm in nonterminals:
            for rule in rules[nonterm]:
                if len(rule) == 0:
                    continue
                #pylint: disable=C0200
                for ind in range(len(rule)):
                    c = rule[ind]
                    if c in terminals:
                        continue
                    if ind == len(rule) - 1:
                        follow_relations[c][nonterm] = True
                        continue
                    c2 = rule[ind + 1]
                    if c2 in terminals:
                        follow_relations[c][c2] = True
                        continue
                    for term in terminals:
                        if first_relations[c2][term]:
                            follow_relations[c][term] = True

        change = True
        while change:
            change = False
            for nonterm in nonterminals:
                for col in nonterminals:
                    if follow_relations[nonterm][col]:
                        for col_check in list(nonterminals) + list(terminals) + ['epsilon']:
                            if follow_relations[col][col_check] and not follow_relations[nonterm][col_check]:
                                change = True
                                follow_relations[nonterm][col_check] = True

        print('firsts:')
        first_per_rule = {}
        for nonterm in nonterminals:
            for rule in rules[nonterm]:
                first_per_rule[rule] = []
                if len(rule) == 0:
                    first_per_rule[rule].append('epsilon')
                for i in rule:
                    if i in terminals:
                        first_per_rule[rule].append(i)
                        break
                    for terminal in terminals:
                        if first_relations[i][terminal]:
                            first_per_rule[rule].append(terminal)
                    j = i
                    next_index = rule.find(j)
                    while j in emptynonterminals:
                        for terminal in terminals:
                            if first_relations[j][terminal]:
                                first_per_rule[rule].append(terminal)
                        next_index += 1
                        if next_index >= len(rule):
                            first_per_rule[rule].append('epsilon')
                            break
                        j = rule[next_index]
                first_per_rule[rule] = set(first_per_rule[rule])

        for nonterm in nonterminals:
            for rule in rules[nonterm]:
                print(f'{nonterm}:{rule} = ', end='')
                for x in first_per_rule[rule]:
                    print(f'{x} ', end='')
                print()

        print('flws:')
        flw_per_nonterm = {}

        for nonterm in nonterminals:
            flw_per_nonterm[nonterm] = []
            for i in list(terminals) + ['epsilon']:
                if follow_relations[nonterm][i]:
                    if i == 'epsilon':
                        flw_per_nonterm[nonterm].append('$')
                        continue
                    flw_per_nonterm[nonterm].append(i)

        for nonterm in nonterminals:
            print(f'{nonterm} = ', end='')
            for flw in flw_per_nonterm[nonterm]:
                print(f'{flw} ', end='')
            print()
